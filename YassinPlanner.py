#!/usr/bin/env python3
'''
TODO:
Fahrplan
Kaffee
Wecker



Mainloop:
- Datum anzeigen/Uhrzeit anzeigen auf companion app
- Raspberry Pi server einrichten
- Verbindung prüfen (DVG)
- Falls Verspätung ansteht entweder Wecker früher einstellen oder warnen
- Sandwichzeit, Toilettenzeit und Kaffeezeit einplanen falls möglich.
    - Toilettenzeit>Kaffeezeit>Sandwichzeit
V 0.0.1
'''
from GetSchedule import Schedule
from getDefaults import GetDefaults
from time import sleep
import os
import sys


class YassinPlanner:
    def mainloop(self,clear_it=False, verbose=True):
        schedule = Schedule()
        default = GetDefaults()
        d = default.get_defaults_all()
        print(d)
        schedule.get_schedule(verbose=True)
        while True:
            conn = schedule.delay_current_connection(verbose=False)
            schedule.get_actual_connection(verbose=True)
            if int(conn)> 0:
                print("Tram is going to be late")
            sleep(5)
            if clear_it:
                self.clear()

    def clear(self):
      os.system("cls" if os.name == "nt" else "clear")



if __name__ == "__main__":
  yassin = YassinPlanner()
  yassin.mainloop(clear_it=True, verbose=False)
