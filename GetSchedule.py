import urllib.request, json


#a few functions to determine wether I'm late or not and other schedule stuff
class Schedule:
    def get_schedule(self,verbose=False):
        with urllib.request.urlopen("https://vrrf.finalrewind.org/Duisburg/Duissern.json?frontend=json&line=U79&platform=%232") as url:
            data = json.loads(url.read().decode())
            if verbose:
                print(json.dumps(data, sort_keys=True, indent=4))
            return data

    def is_tram_too_late(self):
        data = self.get_schedule(verbose=False)
        if int(data["raw"][0]["delay"]) == 0 and data["raw"][0]["countdown"] != "0":
            print("on time")
            return True
        else:
            return False
    def get_actual_connection(self,verbose=True):
        data = self.get_schedule(verbose=False)
        i = 0
        while i <= len(data["raw"]):
            if data["raw"][i]["countdown"] != "0":
                if verbose:
                    print(data["raw"][i]["destination"])
                    print("Scheduled time: " + data["raw"][i]["sched_time"])
                    if data["raw"][i]["sched_time"] != data["raw"][i]["time"]:
                        print("Arrives actually: " + data["raw"][i]["time"])
                return i
            else:
                i = i + 1
    def delay_current_connection(self,verbose):
        i = self.get_actual_connection()
        data = self.get_schedule(verbose=False)
        current_conn = data["raw"][i]["delay"]
        if verbose:
            print(data["raw"][i]["destination"])
        else:
            print("current delay: " + current_conn + " minutes\n")
        return current_conn
if __name__ == "__main__":
    s = Schedule()
    #s.get_schedule(verbose=True)
    s.get_actual_connection()
    if s.is_tram_too_late():
        print("tOO LATE")
    else:
        print("ezpz")
