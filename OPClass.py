import sqlite3
from YassinPlanner import YassinPlanner
class OP:
    name = "blank"
    status = "blank"
    body = "blank"
    id = "blank"
    def __init__(self, name, status, body,id):

        self.name = name
        self.body = body
        self.status = status
        self.id = str(id)



class OPCollection:
    id = 0
    OPS = []

    def getID(self):
        return len(self.OPS)
    #def createConnection(self):


    def addOP(self,op):
        self.OPS.append(op)

    def addNewOP(self, name,body, status):
        op = OP(name,body,status,self.getID())
        self.OPS.append(op)


    def showAllOPS(self):
        for op in self.OPS:
            print(op.id + " | " + op.name + " | "+ op.body + " | " + op.status  )

    def getOP(self,id,verbose=False):
        op = self.OPS[id]
        if verbose:
            print(op.id + " | " + op.name + " | "+ op.body + " | " + op.status  )
        return op

o = OPCollection()
y = YassinPlanner()
i = -1
while True:
    i = i+1
    #o.addNewOP("name","body","active")
    choice=input()
    if choice == "name":
        print(o.getOP(i).name)
    elif choice == "body":
        print(o.getOP(i).status)
    elif choice == "status":
        print(o.getOP(i).body)
    elif choice == "add":
        name = input("name: ")
        body = input("body: ")
        status = input("status: ")
        o.addNewOP(name,body,status)
        print(str(o.OPS[len(o.OPS)-1].id) + " is the ID of new OP" )
    elif choice == "showOp":
        try:
            opcode = int(input("ID: "))
            o.getOP(opcode,verbose=True)
        except IndexError:
            print("There is no Operation with that OPCode")
    else:
        try:
            print(str(i) + " " + str(len(o.OPS)))
            o.getOP(i, verbose=True)
        except IndexError:
            print("error")
    input("Press Enter...")
    y.clear()
