from configparser import ConfigParser

class GetDefaults:
    def __init__(self):
        self.con = ConfigParser()
        self.con.read("YassConfig.ini")
        self.con.sections()

    def get_default(self,section,value):
        '''Returns a specific Default as Tuple '''
        return (value,self.con[section][value])
    def get_defaults_all(self):
        '''Returns a list of all Defaults'''
        all_defaults = []
        i = 0
        for key in self.con["DEFAULTS"]:
            all_defaults.append(key)
            print(self.get_default("DEFAULTS",key))
            i = i + 1
        return all_defaults

if __name__ == "__main__":
    D = GetDefaults()
    #D.get_defaults_all()
    f = D.get_default(section="DEFAULTS", value="latest_wake_up_time")
